function [fringe, fov, fail] = ...
    getFringeAndFOV(cal_path, cal_fname, fringe_field, dlm)
%getFringe returns the fringe from a desinusoid file

% Defaults
fail = false;
fringe = [];
fov = [];

% Constants
FOV_UNIT = 'deg';

% Try to load desinusoid file
try
    dsin = load(fullfile(cal_path, cal_fname));
catch
    fail = true;
    return;
end
% Look for expected field
if isfield(dsin, fringe_field)
    fringe = dsin.horizontal_fringes_fringes_period;
    
    % Try to determine FOV from filename, should work on all modern
    % desinusoid files (as of 2018.10.26)
    try
        fname_parts = strsplit(cal_fname, dlm);
        fov_str = fname_parts{find(strcmpi(fname_parts, FOV_UNIT)) -1};
        fov = str2double(strrep(fov_str, 'p', '.'));
    catch
        fail = true;
    end
else
    fail = true;
end

end

