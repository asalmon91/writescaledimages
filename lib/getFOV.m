function [fov, fail] = getFOV(vidnum, vid_path, vid_heads, fov_field)
%getFOV given a video number, a list of all possible video headers, as well
%as the path to those headers, and the field name which indicates that it
%has the FOV information

fail = false;
fov_found = false(size(vid_heads));
for jj=1:numel(vid_heads)
    % Find the first .mat that matches the video number
    if ~isempty(strfind(vid_heads{jj}, vidnum))
        head = load(fullfile(vid_path, vid_heads{jj}));
        % And has the fieldname we're looking for
        if isfield(head, fov_field)
            fov = head.optical_scanners_settings. ...
                raster_scanner_amplitude_in_deg;
            fov_found(jj) = true;
            break;
        end    
    end
end
if ~any(fov_found)
    fail = true;
end


end

