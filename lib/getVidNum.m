function [vidnum, fail] = getVidNum(fname, mod_labels, dlm, n_digs_vidnum)
%getVidNum Extracts the video number from an AOSLO filename

% Defaults
fail = false;
vidnum = '0';

% Remove extension from filename
fname = fname(1: end - strfind(flip(fname), '.'));

% Look for modality label in filename
char_found = false(numel(mod_labels), 1);
for jj=1:numel(mod_labels)
    char_index = strfind(fname, mod_labels{jj});
    % Mod label should only occur once in filename
    if ~isempty(char_index) && numel(char_index) == 1
        char_found(jj) = true;
    end
end

% It should also only match one modality label
if numel(find(char_found)) == 1
    % Split by delimeter and take the next token
    fname_parts = strsplit(fname, dlm);
    vidnum = fname_parts{find(strcmpi(...
        fname_parts, mod_labels{char_found})) ...
        +1};
    % Check that it matches expected format
    if numel(vidnum) ~= n_digs_vidnum || ...
            isnan(str2double(vidnum))
        fail = true;
    end
else
    fail = true;
end

end

