# writeScaledImages
Created by Alex Salmon - AOIP - 2018.10.26

## Warnings:
* Only intended for use with AOSLO images
* Can serve as a pre-processing step for automatic montaging programs that don't support multiple FOV's at once
* Does not currently support multiple imaging wavelengths

**Assumes several things about file names and object fields**
* video numbers immediately follow modality label and is a zero-padded 4-digit number
* fringe values stored in desinusoid matrix files in field: "horizontal_fringes_fringes_period"
* field-of-view values stored in video header file in field: "optical_scanners_settings.raster_scanner_amplitude_in_deg"
* Some of these constants can be changed in the first few lines of writeScaledImages.m if your system differs

## Prerequisites
* Matlab
    * Developed and tested in R2017b only

## Process
1. Select images (.tif)
2. Select path to videos and video headers (.mat)
3. Select desinusoid matrix files (desinusoid_matrix*.mat)
4. Inspect files for approximately appropriate size in scaled folder within original tif folder

## Supporting functions may be of use to AOSLO users
* getVidNum:
    * extracts video number from filename, requires list of possible modalities, token delimiter (_), number of digits expected in zero-padded video number
    * should work on .tif's and .avi's
* getFOV
    * extracts the field-of-view from a video header given the video number returned by getVidNum, path to the video headers, a list of all possible header files in a cell array, and the expected field which indicates the .mat file will possess the FOV information
* getFringe:
    * extracts the fringe period of a desinusoid matrix file given the path and filename, the expected field name containing fringe information, and the token delimiter
