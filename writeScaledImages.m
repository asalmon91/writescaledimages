%% WriteScaledImages
% Pre-processing step for automontaging that doesn't currently handle 
% multiple FOVs
% Created 2018.10.26 - Alex Salmon

%% Import
addpath('.\lib');

%% Constants
% Video number follows modality label, best way I know so far to determine
% video number from a tif
MOD_LABELS = {
    'confocal';
    'det'; % can't use split_det because of the _
    'avg';
    'direct';
    'reflect';
    'visible';
    'binary'};
DLM = '_'; % delimiter
N_DIGS_VIDNUM = 4; % number of digits in video number
FOV_FIELDNAME = 'optical_scanners_settings';
FRINGE_FIELDNAME = 'horizontal_fringes_fringes_period';
INTERP_METHOD = 'bicubic';

%% Get .tif's, path to video headers, and calibration files
% Images
[tif_fnames, tif_path] = uigetfile('*.tif', 'Select images to montage', ...
    'multiselect', 'on');
if all(tif_path == 0)
    return;
elseif ~iscell(tif_fnames)
    tif_fnames = {tif_fnames};
end
tif_fnames = tif_fnames';
% Video path
vid_path = uigetdir(tif_path, 'Select folder containing videos');
if all(vid_path) == 0
    return;
end
% Calibration files
[cal_fnames, cal_path] = uigetfile('*desinusoid_matrix*.mat', ...
    'Select all desinusoid files', vid_path, 'multiselect', 'on');
if all(cal_path == 0)
    return;
elseif ~iscell(cal_fnames)
    cal_fnames = {cal_fnames};
end
cal_fnames = cal_fnames';

%% Waitbar
wb = waitbar(0, sprintf('Processing %s..................', tif_fnames{1}));
wb.Children.Title.Interpreter = 'none';

%% For each image, determine video number
wb.Name = 'Getting video numbers';
vidnums = cell(size(tif_fnames));
for ii=1:numel(tif_fnames)
    % Get video number
    [vidnum, fail] = getVidNum(tif_fnames{ii}, ...
        MOD_LABELS, DLM, N_DIGS_VIDNUM);
    if fail
        warning('Video number not found for %s', tif_fnames{ii});
        continue;
    end
    vidnums{ii} = vidnum;
    
    waitbar(ii/numel(tif_fnames), wb, tif_fnames{ii});
end
clear vidnum

%% For each video, determine FOV
wb.Name = 'Getting FOVs';
% Get unique video numbers
[u_vidnum, ~, ic] = unique(vidnums);
% Load all .mats in video folder, format into cell array
vid_heads = dir(fullfile(vid_path, '*.mat'));
vid_heads = {vid_heads.name}';
fovs = zeros(size(u_vidnum));
for ii=1:numel(u_vidnum)
    % Get FOV
    [fov, fail] = getFOV(u_vidnum{ii}, vid_path, vid_heads, FOV_FIELDNAME);
    if fail
        warning('FOV not found for %s', tif_fnames{ii});
        continue;
    end
    fovs(ii) = fov;
    waitbar(ii/numel(u_vidnum), wb, u_vidnum{ii});
end
clear fov
fovs = fovs(ic); % return to original index

%% For each FOV, determine fringe
wb.Name = 'Getting fringes';
% [u_fov, ia, ic] = unique(fovs);
fringes = zeros(size(cal_fnames));
cal_fovs = fringes;
for ii=1:numel(cal_fnames)
    [fringe, fov, fail] = getFringe(cal_path, cal_fnames{ii}, ...
        FRINGE_FIELDNAME, DLM);
    if fail
        warning('Fringe not found for %s',cal_fnames{ii});
        continue;
    end
    fringes(ii) = fringe;
    cal_fovs(ii) = fov;
    
    waitbar(ii/numel(cal_fnames), wb, cal_fnames{ii});
end
clear fringe fov

%% Use fringe to calculate scaling factor
scale_factors = max(fringes)./fringes;

%% Format output
out_path = fullfile(tif_path, 'scaled');
if exist(out_path, 'dir') == 0
    mkdir(out_path);
end

%% Scale images
wb.Name = 'Writing';
for ii=1:numel(tif_fnames)
    % Get scale factor
    try
        imwrite(imresize(imread(fullfile(tif_path, tif_fnames{ii})), ... % Reading
            scale_factors(fovs(ii) == cal_fovs), INTERP_METHOD), ... % Scaling
            fullfile(out_path, strrep(tif_fnames{ii}, '.tif', '_scl.tif'))); % Writing
    catch
        warning('Failed to scale %s', tif_fnames{ii});
    end
    waitbar(ii/numel(tif_fnames), wb, tif_fnames{ii});
end

close(wb);







